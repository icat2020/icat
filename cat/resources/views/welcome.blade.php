<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Info-CAT</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">



        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="center">
                <img src="/storage/images/cat_blue.png">
            </div>

            <div class="content">
                <div class="title m-b-md">
                    Info CAT
                </div>

                <div class="links">
                    <p><strong>Mantén un control de tus ventas</strong></p>
                    <p><strong>y ayuda a tus clientes a saber su estado de cuenta.</strong></p>
                    <hr>
                    <p><strong>Con Info CAT tendrás el control de tus clientes</strong></p>
                    <p><strong>Cúal es el estado de su cuenta y les permitirá saber cómo</strong></p>
                    <p><strong>van sus abonos a el día</strong></p>
                    <hr>                    
                    <table class="egt">
                        <tr>
                            <td><i class="material-icons" style="font-size:48px;color:purple">forward</i></td>
                            <td><p><strong>Lo único que necesitan es solicitar un código de estado de cuenta a la persona con la cual tienes el crédito</strong></p></td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                    <p><strong>entrar a la sección de estado de cuenta</strong></p>
                    <p><strong>poner el código de estado y tendrás a el momento</strong></p>
                    <p><strong>toda la información sobre tus abonos</strong></p>
                </div>
            </div>
        </div>
    @extends('extras.tidio_chat')
    </body>
</html>
