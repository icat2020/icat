

@extends('layouts.app')

@section('content')
<div class="container">
	@foreach ($data_user as $object)

	    <!---<div class="row justify-content-center">--->
	    <div class="row justify-content-left">
	        <div class="col-md-6">
	            <div class="card">
	                <div class="card-header p-3 mb-2 bg-info text-white">{{ __('My Profile') }}</div>

	                <div class="card-body">

	                   
	                        @csrf
	                        <div class="form-group row">
	                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
	                            <label for="user_name" class="col-md-4 col-form-label text-md-right">{{ $object->name }}</label>
	                        </div>


	                        <div class="form-group row">
	                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name Business') }}</label>
	                            <label for="user_name" class="col-md-6 col-form-label text-md-right">{{ $object->name_business }}</label>
	                        </div>

	                        <div class="form-group row">
	                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
	                            <label for="user_name" class="col-md-6 col-form-label text-md-right">{{ $object->email }}</label>
	                        </div>


	                        <div class="form-group row mb-0">
	                            <div class="col-md-6 offset-md-4">
	                                <button type="submit" class="btn btn-primary">
	                                    {{ __('Edit') }}
	                                </button>
	                            </div>
	                        </div>

	                </div>
	                </div>
	            </div>
	        </div>
	    </div>
    @endforeach
</div>

<div class="container">
	@foreach ($data_user as $object)

	    <!---<div class="row justify-content-center">--->
	    <div class="row justify-content-right">
	        <div class="col-md-6">
	            <div class="card">
	                <div class="card-header p-3 mb-2 bg-info text-white">{{ __('My Profile') }}</div>

	                <div class="card-body">
	                <div class="card-body">
	                   
	                        @csrf

	                        <div class="form-group row">
	                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

	                            <div class="col-md-6">
	                                <input  type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $object->name }}" required autocomplete="name" autofocus>

	                                @error('name')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                        </div>

	                        <div class="form-group row">
	                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

	                            <div class="col-md-6">
	                                <input  type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $object->email }}" required autocomplete="email" autofocus>

	                                @error('email')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
	                            </div>
	                        </div>


	                        <div class="form-group row mb-0">
	                            <div class="col-md-6 offset-md-4">
	                                <button type="submit" class="btn btn-primary">
	                                    {{ __('Edit') }}
	                                </button>
	                            </div>
	                        </div>

	                </div>
	                </div>
	            </div>
	        </div>
	    </div>
    @endforeach
</div>
@endsection


@extends('extras.tidio_chat')
@extends('extras.icons_google')