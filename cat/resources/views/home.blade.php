@extends('layouts.app')

@section('content')
<div class="container">
    <div class="flas_message">
        @foreach(['danger','warning','success','info'] as $msg)
            @if(Session::has('alert-'.$msg))
                <p class="alert alert-{{$msg}}">
                    {{ Session::get('alert-'.$msg) }}
                    <a href="#" class="close" data-dismiss="alert" arial-label="close">&times;</a>
                </p>
            @endif
        @endforeach
    </div>
    <!---<div class="row justify-content-center">--->
    <div>
        <div class="col-md-15">
            <div class="card">
                <div class="card-header p-3 mb-2 bg-info text-white">{{ __('Client') }}</div>

                <div class="card-body">
                    <!---@if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}--->
                    <show-all-clients-component :uid="{{ Auth::user()->id }}"></show-all-clients-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('extras.tidio_chat')
@extends('extras.icons_google')

