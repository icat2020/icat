<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes(['verify' => true]);

/**Route::get('/home', 'HomeController@index')->name('home');**/

Auth::routes();
//     ------------------ Home ----------------
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/verified/{uid}','HomeController@verified');
Route::get('lang/{lang}', 'LanguageController@swap')->name('lang.swap');

//-------------------Clients----------------------

Route::get('/new-client', 'ClientController@addclient')->name('new-client');
Route::post('/add/client', 'ClientController@add')->name('add-client');


// ------------------- Users ---------------------
Route::get('/profile','UserController@profile')->name('profile');

