<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clients extends Model
{
	use Notifiable;
    use SoftDeletes;

    
    protected $table = 'clients';

    public function users(){
        return $this->belongsToMany(User::class);
    }
}
