<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use Auth;


class UserController extends Controller
{
    public function profile(Request $request){
    	$uid=Auth::user()->id;
    	$data_user=User::find($uid)->get();
    	return view('user.profile')->with('data_user', $data_user);
    	//return($data_user);
    }
}
