<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
Use Mail;
use App\Mail\Welcome;
use App\Mail\MailNotify;
use App\Mail\WelcomeMail;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function verified(Request $equest, $uid){
        $user=User::find($uid);
        //dd($user->email);
        Mail::to($user->email)->send(new WelcomeMail($user));
    }
}
