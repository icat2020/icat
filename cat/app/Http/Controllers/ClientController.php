<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Clients;

use Auth;

class ClientController extends Controller
{
    public function addclient(){
    	return view('clients.addclient');
    }

    public function add(Request $request){
    	$uid=Auth::user()->id;
        DB::beginTransaction();
        try {
            $client = new Clients();
            $client->user_id = $uid;
            $client->name = $request->get('name');
            $client->phone = $request->get('phone');
            $client->direction = $request->get('direction');  
            $client->save();

            DB::commit();
            
            $request->session()->flash('alert-success','Client Saved Succesfull');
            return redirect()->route('home');



        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('status' => false, 'message' => __($e->getMessage())));
        }
    }
}
