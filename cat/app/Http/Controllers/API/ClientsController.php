<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Clients;

class ClientsController extends Controller
{
    public function index(Request $request, $uid){
    	$clients=Clients::where('user_id',$uid)->where('deleted_at',null)->get();
    	return response()->json(array('status' => true, 'message' => 'true', 'clients' => $clients));
    }

    public function admin(Request $request){
    	$all_clients=Clients::where('deleted_at',null)->get();
    	return($all_clients);
    }

   public function delete(Request $request, $id){
   	$client=Clients::findOrFail($cid)->delete();
   	return response()->json(array('status' => true, 'message' => 'true', 'clients' => $clients));
   }
}
